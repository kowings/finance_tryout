#include "main.hpp"
#include "finance.hpp"
#include "connec.hpp"

int main(int argc, char* argv[])
{
	//Inside main should only contain the following process.
	//1) Show introduction
	//2) Connect to sql
	//3) Run Finance program
	//4) Disconnect sql
	//5) Exit program
	
	introduction();
	connectToMySql();
	RunFinance();
	disconnectFromMysql();
	
	return 0;
}
