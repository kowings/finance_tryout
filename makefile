CC = g++
CFLAGS = -g -Wall

all = finance

finance: main.o finance.o connec.o
	$(CC) $(CFLAGS) -o all main.o finance.o connec.o
	
main.o: main.cpp main.hpp finance.hpp
	$(CC) $(CFLAGS) -c main.cpp
	
finance.o: finance.cpp finance.hpp
	$(CC) $(CFLAGS) -c finance.cpp
	
connec.o: connec.cpp connec.hpp
	$(CC) $(CFLAGS) -c connec.cpp
	
clean: 
	rm all *.o 
